"""
Реализуйте функцию которая умеет работать с файлами (читать из заданного файла,
записывать в заданный файл). Программа считает количество строк, слов и букв в
заданном файле и дописывает эту информацию обратно в файл, так же выводит эту
информацию на экран.
"""
import re


def f(name_file):
    lines = 0
    letters = 0
    worlds = 0
    a = open(name_file)
    for line in a:
        lines += 1
        i_line = line
        re_line = re.findall('[a-zA-Z]', line)
        list_line = i_line.split()
        worlds += len(list_line)
        letters += len(re_line)
    a.close()

    a = open(name_file, 'a')
    a.write(f'\n{lines}, {worlds}, {letters}')
    a.close()
    print(lines, worlds, letters)


f('mytxt.txt')
