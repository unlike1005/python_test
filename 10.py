"""
10*. Реализуйте класс Purchase который хранит в себе список покупок и их
итоговую стоимость. Создаваться объект класса может, например, так
>> purchase = Purchase(10, 'pen', 'book', 'pencil')
Можно красиво напечатать сумму покупок и что в корзине
>> print(purchase)
Получить кол-во покупок в корзине
Сложить стоимость 2-х корзин
>> purchase_1 + purchase_2
Прибавить к итоговой стоимости корзины какое-то число(int, float)
>> purchase + 1.2
>> 1 + purchase
Добавить в корзину товар
>> ("mango", 1) + purchase1
>> purchase + ("mango", 1)
"""


class Purchase:
    def __init__(self, summa, *goods):
        self.summa = summa
        self.goods = goods

    def __repr__(self):
        return 'Purchase({!r} {!r})'.format(self.summa, self.goods)

    def __add__(self, other):
        if isinstance(other, int) or isinstance(other, float):
            self.summa = self.summa + other
        elif isinstance(other, Purchase):
            self.summa = self.summa + other.summa
            self.goods = self.goods + other.goods
        elif isinstance(other, tuple):
            list_other = []
            string_goods = list(self.goods)
            for i in other:
                list_other.append(i)
            if isinstance(other[0], int) or isinstance(other[0], float):
                string_goods.append(list_other[1])
                self.summa = self.summa + list_other[0]
            else:
                string_goods.append(list_other[0])
                self.summa = self.summa + list_other[1]
            self.goods = tuple(string_goods)
        return Purchase.__repr__(self)

    def __radd__(self, other):
        return Purchase.__add__(self, other)


purchase = Purchase(5, 'pen', 'book', 'pencil')
purchase_1 = Purchase(10, 'pen', 'book', 'pencil')
purchase_2 = Purchase(10, 'pen', 'book', 'pencil')
print(purchase_1 + purchase_2)
print(purchase + 1)
print(3 + purchase)
print(purchase_1 + ("mango", 1))
print(("mango", 1) + purchase_1)
