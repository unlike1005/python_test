"""
Напишите декоратор с параметрами decorator, который делает так,
что задекорированная функция, будет возвращать результат в следующем формате:
аргументы распечатаны в виде пары: кортеж и словарь

@decorator(1, 2, 3, [1, 2, 3], 'one', 'two', 'three', one=1, two=2, three=3)
def identity(x):
  return x

print(identity(42))

>>> ((1, 2, 3, [1, 2, 3], 'one', 'two', 'three'),
{'two': 2, 'one': 1, 'three': 3})
>>> 42

@decorator()
def identity(x):
  return x

print(identity(42))

>>> ((), {})
>>> 42
"""


def decorator(*a, **b):
    def dec(identity):
        def wrapper(x):
            if a:
                li = []
                for i in a:
                    li.append(i)
                print(li, b)
            return identity(x)
        return wrapper
    return dec


@decorator(1, 2, 3, [1, 2, 3], 'one', 'two', 'three', one=1, two=2, three=3)
def identity(x):
    return x


print(identity(42))
