"""
Напишите функцию, преобразующую входную строку в выходную как в примерах,
Пусть s = "abcdef...xyz", тогда вывод будет таким:
f(s, 1) => "a"
f(s, 2) => "aba"
f(s, 3) => "abcba"
f(s, 4) => "abcdcba"
"""
s = "abcdef...xyz"


def f(s: str, n: int):
    return s[:n] + s[:n - 1][::-1]


print(f(s, 1))
print(f(s, 2))
print(f(s, 3))
print(f(s, 4))
