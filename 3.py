"""
Свяжите переменную с списком который состоит из элементов от 1 до 10
Извлеките из списка элементы стоящие на нечетных позициях
Найдите максимальное число
Найдите минимальное число
Найдите сумму всех чисел заданного списка
Из списка вида [[1, 2, 3], [4, 5, 6], [7, 8, 9, 10]].
Напечатайте только первые элементы вложенных списков, те 1 4 7.
При помощи цикла for.
"""
some_list = list(range(1, 11, 1))
print(some_list)
print(some_list[0:9:2])
print(max(some_list))
print(min(some_list))
print(sum(some_list))
new_list = [[1, 2, 3], [4, 5, 6], [7, 8, 9, 10]]
for i in new_list:
    print(i[0])
