"""
Напишите функцию, которая принимает строку и возвращает кол-во
заглавных и строчных букв.
'The quick Brow Fox' =>
Upper case characters: 3
Lower case сharacters: 12
"""
a = 'The quick Brow Fox'
upper_letters = 0
lower_letters = 0
for i in range(len(a)):
    if a[i].islower():
        lower_letters += 1
    elif a[i].isupper():
        upper_letters += 1
    else:
        i += 1
    i += 1
print(f'Upper case characters: {upper_letters}\n'
      f'Lower case сharacters: {lower_letters}')
