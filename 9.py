"""
Создайте 3 класса: Animal, Cow, Dog. Классы Cow, Dog наследуются от класса
Animal.
У всех классов есть методы: swim, say. Дополните методы родительского класса.

Например, метод родительского класса say печатает:
>> print(f"{self.type} wants to say smth")
Метод дочернего класса say печатает:
>> print(f"{self.type} {self.name} says meow")

>> cat = Cat("Vasya")
>> cat.say()

>> Cat wants to say smth
>> Cat Vasya says meow
"""


class Animal:
    def __init__(self, name):
        self.name = name
        self.type = None

    def say(self):
        print(f"{self.type} wants to say hello")

    def swim(self):
        print(f"{self.type} can swim")


class Cat(Animal):
    def __init__(self, name):
        super().__init__(name)
        self.type = Cat.__name__

    def say(self):
        Animal.say(self)
        print(f"{self.type} {self.name} says meow")

    def swim(self):
        print(f"{self.type} can't swim")


class Dog(Animal):
    def __init__(self, name):
        super().__init__(name)
        self.type = Dog.__name__

    def say(self):
        Animal.say(self)
        print(f"{self.type} {self.name} says gav")


cat = Cat("Marsik")
cat.say()
cat.swim()

dog = Dog("Bobik")
dog.say()
dog.swim()
